﻿namespace GeometryDrawer
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.ClearPlotButton = new System.Windows.Forms.Button();
            this.drawnFiguresList1 = new GeometryDrawer.UserControls.DrawnFiguresList();
            this.toolParametersPanel1 = new GeometryDrawer.UserControls.ToolParametersPanel();
            this.plot1 = new GeometryDrawer.UserControls.Plot();
            this.geometryToolPanel1 = new GeometryDrawer.UserControls.GeometryToolPanel();
            this.actionToolPanel1 = new GeometryDrawer.UserControls.ActionToolPanel();
            this.SuspendLayout();
            // 
            // ClearPlotButton
            // 
            this.ClearPlotButton.Location = new System.Drawing.Point(484, 498);
            this.ClearPlotButton.Name = "ClearPlotButton";
            this.ClearPlotButton.Size = new System.Drawing.Size(151, 36);
            this.ClearPlotButton.TabIndex = 4;
            this.ClearPlotButton.Text = "Clear plot";
            this.ClearPlotButton.UseVisualStyleBackColor = true;
            this.ClearPlotButton.Click += new System.EventHandler(this.ClearPlotButton_Click);
            // 
            // drawnFiguresList1
            // 
            this.drawnFiguresList1.Location = new System.Drawing.Point(484, 68);
            this.drawnFiguresList1.Name = "drawnFiguresList1";
            this.drawnFiguresList1.Size = new System.Drawing.Size(151, 397);
            this.drawnFiguresList1.TabIndex = 5;
            // 
            // toolParametersPanel1
            // 
            this.toolParametersPanel1.Location = new System.Drawing.Point(72, 443);
            this.toolParametersPanel1.Name = "toolParametersPanel1";
            this.toolParametersPanel1.Size = new System.Drawing.Size(362, 100);
            this.toolParametersPanel1.TabIndex = 3;
            // 
            // plot1
            // 
            this.plot1.BackColor = System.Drawing.Color.White;
            this.plot1.Location = new System.Drawing.Point(72, 68);
            this.plot1.Name = "plot1";
            this.plot1.Size = new System.Drawing.Size(362, 358);
            this.plot1.TabIndex = 2;
            // 
            // geometryToolPanel1
            // 
            this.geometryToolPanel1.Location = new System.Drawing.Point(10, 68);
            this.geometryToolPanel1.Name = "geometryToolPanel1";
            this.geometryToolPanel1.Size = new System.Drawing.Size(56, 332);
            this.geometryToolPanel1.TabIndex = 1;
            // 
            // actionToolPanel1
            // 
            this.actionToolPanel1.Location = new System.Drawing.Point(286, 21);
            this.actionToolPanel1.Name = "actionToolPanel1";
            this.actionToolPanel1.Size = new System.Drawing.Size(148, 41);
            this.actionToolPanel1.TabIndex = 6;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 643);
            this.Controls.Add(this.actionToolPanel1);
            this.Controls.Add(this.drawnFiguresList1);
            this.Controls.Add(this.ClearPlotButton);
            this.Controls.Add(this.toolParametersPanel1);
            this.Controls.Add(this.plot1);
            this.Controls.Add(this.geometryToolPanel1);
            this.DoubleBuffered = true;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private UserControls.GeometryToolPanel geometryToolPanel1;
        private UserControls.Plot plot1;
        private UserControls.ToolParametersPanel toolParametersPanel1;
        private System.Windows.Forms.Button ClearPlotButton;
        private UserControls.DrawnFiguresList drawnFiguresList1;
        private UserControls.ActionToolPanel actionToolPanel1;
    }
}

