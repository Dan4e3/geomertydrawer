﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeometryDrawer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            geometryToolPanel1.SetToolParametersPanel(toolParametersPanel1);
            toolParametersPanel1.SetRelatedGeometryToolPanel(geometryToolPanel1);
            plot1.SetRelatedPanel(geometryToolPanel1, 
                                toolParametersPanel1, 
                                drawnFiguresList1,
                                actionToolPanel1);
            drawnFiguresList1.SetRelatedPlot(plot1);
            actionToolPanel1.SetRelatedPlot(plot1);
        }

        private void ClearPlotButton_Click(object sender, EventArgs e)
        {
            plot1.ClearPlot();
        }
    }
}
