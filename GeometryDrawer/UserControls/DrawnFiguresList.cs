﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static GeometryDrawer.UserControls.Plot;

namespace GeometryDrawer.UserControls
{
    public partial class DrawnFiguresList : UserControl
    {
        private ImageList ImList { get; set; }
        public Plot RelatedPlot { get; private set; }
        private bool _suppressItemSelEvent = false;
        public event FigureListChangedEventHandler FigureRemoved;

        public DrawnFiguresList()
        {
            InitializeComponent();
            ImList = new ImageList();
            DrawnFiguresListView.SmallImageList = ImList;
            //ImList.Images.Add(Circle.Tag, Circle.CreateDefaultIcon(16, 16));
            //ImList.Images.Add(Rectangle.Tag, Rectangle.CreateDefaultIcon(16, 16));
            //ImList.Images.Add(Square.Tag, Square.CreateDefaultIcon(16, 16));
        }

        public void SetRelatedPlot(Plot plt)
        {
            RelatedPlot = plt;
            plt.FigureAddedToPlot += Plt_FigureAddedToPlot;
        }

        private void Plt_FigureAddedToPlot(object sender, FigureEventArgs e)
        {
            Figure fig = e.SubjectFigure;
            ImList.Images.Add(fig.Name, fig.CreateCurrentIcon());
            ListViewItem itm = new ListViewItem(fig.Name)
            {
                ImageKey = fig.Name,
                Tag = fig
            };
            DrawnFiguresListView.Items.Add(itm);
        }

        private void DrawnFiguresListView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && DrawnFiguresListView.SelectedItems.Count > 0)
            {
                foreach (ListViewItem itm in DrawnFiguresListView.SelectedItems)
                {
                    RemoveItemFromList(itm);
                }
            }

            else if (e.KeyCode == Keys.A && e.Modifiers == Keys.Control)
            {
                DrawnFiguresListView.Select();
                foreach (ListViewItem itm in DrawnFiguresListView.Items)
                    itm.Selected = true;
            }
        }

        public void RemoveFigureFromList(Figure fig)
        {
            ListViewItem assocItem = DrawnFiguresListView.Items.Cast<ListViewItem>().First(itm => itm.Tag.Equals(fig));
            RemoveItemFromList(assocItem);
        }

        private void RemoveItemFromList(ListViewItem itmToRemove)
        {
            FigureRemoved?.Invoke(this, new FigureEventArgs((Figure)itmToRemove.Tag));
            itmToRemove.Remove();
            ImList.Images.RemoveByKey(itmToRemove.Name);
        }

        public void ClearList()
        {
            DrawnFiguresListView.Items.Clear();
            ImList.Images.Clear();
        }

        private void DrawnFiguresListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (_suppressItemSelEvent)
                return;
            Figure fig = (Figure)e.Item.Tag;
            if (e.IsSelected)
                fig.Select();
            else
                fig.Deselect();
        }

        public void ChangeFigureItemState(Figure fig, bool state, bool suppressItemSelEvent = true)
        {
            _suppressItemSelEvent = suppressItemSelEvent;
            ListViewItem itm = DrawnFiguresListView.Items.Cast<ListViewItem>().First(x => x.Tag.Equals(fig));
            itm.Selected = state;
            _suppressItemSelEvent = false;
        }
    }
}
