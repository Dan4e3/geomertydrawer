﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RectangleSys = System.Drawing.Rectangle;

namespace GeometryDrawer.UserControls
{
    //TODO Добавить возможность менять толщину линий
    //TODO Добавить массовый выбор объектов при зажатой ЛКМ и движении курсора - как иконки на рабочем столе
    //TODO При множественной выборке объектов - ресайзить их все, а не только первый выделенный
    //TODO При изменении размеров объекта и выноса точки в противоположную сторону рисовка фигуры исчезает
    //TODO Добавить фиксированный ресайзинг квадрата/круга (а также других кастомных фигур, надо сделать через общую реализацию)

    public partial class GeometryToolPanel : UserControl
    {
        private FigureFactory _activeFactory = null;
        public FigureFactory ActiveFactory {
            get { return _activeFactory; }
            private set
            {
                _activeFactory = value;
                if (value != null)
                {
                    _activeFactory.UpdateSize(ParametersPanel.HorSize, ParametersPanel.VerSize);
                    _activeFactory.UpdateFill(ParametersPanel.IsFilled);
                    _activeFactory.UpdateFillColor(ParametersPanel.SelectedFillColor);
                    _activeFactory.UpdateContourColor(ParametersPanel.SelectedContourColor);
                }
            }
        }
        public ToolParametersPanel ParametersPanel { get; private set; }
        public event EventHandler GeometryToolSelectionChanged;

        public GeometryToolPanel()
        {
            InitializeComponent();
        }
        public void SetToolParametersPanel(ToolParametersPanel toolParamsPanel)
        {
            ParametersPanel = toolParamsPanel;
        }

        private void CircleButton_Click(object sender, EventArgs e)
        {
            ActiveFactory = new CircleFactory();
            GeometryToolSelectionChanged?.Invoke(this, new EventArgs());
        }

        private void RectangleButton_Click(object sender, EventArgs e)
        {
            ActiveFactory = new RectangleFactory();
            GeometryToolSelectionChanged?.Invoke(this, new EventArgs());
        }

        private void SquareButton_Click(object sender, EventArgs e)
        {
            ActiveFactory = new SquareFactory();
            GeometryToolSelectionChanged?.Invoke(this, new EventArgs());
        }

        public void DeactivateFactory()
        {
            ActiveFactory = null;
        }
    }

    public delegate void FigureStateChange(object sender, FigureStateChangedEventArgs e);
    public class FigureStateChangedEventArgs: EventArgs
    {
        public bool? IsSelected { get; set; } = null;
        public bool? IsResized { get; set; } = null;
    }

    public abstract class FigureFactory
    {
        protected bool IsFilled { get; set; } = false;
        protected Color FillColor { get; set; } = Color.Black;
        protected Color ContourColor { get; set; } = Color.Black;
        public abstract Figure CreateFigure();
        public abstract Cursor GetCursor();
        public abstract void UpdateSize(double horValue, double verValue);
        public virtual void UpdateFill(bool isFilled)
        {
            IsFilled = isFilled;
        }
        public void UpdateFillColor(Color color)
        {
            FillColor = color;
        }
        public void UpdateContourColor(Color color)
        {
            ContourColor = color;
        }
    }

    public class CircleFactory : FigureFactory
    {
        private static int Id = 1;
        private double Radius { get; set; } = 6;
        public override Figure CreateFigure()
        {
            Circle fig = new Circle("circle" + Id++, Radius)
            {
                IsFilled = IsFilled,
                FillColor = FillColor,
                ContourColor = ContourColor
            };
            fig.DrawPen.Color = ContourColor;
            fig.DrawBrush = new SolidBrush(FillColor);
            return fig;
        }
        public override Cursor GetCursor()
        {
            return new Circle("", Radius).HoverCursor;
        }

        public override void UpdateSize(double radius, double dummy = 0)
        {
            Radius = radius;
        }
    }

    public class RectangleFactory : FigureFactory
    {
        private static int Id = 1;
        private double HorSide { get; set; } = 10;
        private double VerSide { get; set; } = 5;
        public override Figure CreateFigure()
        {
            Rectangle fig = new Rectangle("rectangle" + Id++, VerSide, HorSide)
            {
                IsFilled = IsFilled,
                FillColor = FillColor,
                ContourColor = ContourColor
            };
            fig.DrawPen.Color = ContourColor;
            fig.DrawBrush = new SolidBrush(FillColor);
            return fig;
        }
        public override Cursor GetCursor()
        {
            return new Rectangle("", VerSide, HorSide).HoverCursor;
        }

        public override void UpdateSize(double horValue, double verValue)
        {
            HorSide = horValue;
            VerSide = verValue;
        }
    }

    public class SquareFactory : FigureFactory
    {
        private static int Id = 1;
        private double Side { get; set; } = 10;
        public override Figure CreateFigure()
        {
            Square fig = new Square("square" + Id++, Side)
            {
                IsFilled = IsFilled,
                FillColor = FillColor,
                ContourColor = ContourColor
            };
            fig.DrawPen.Color = ContourColor;
            fig.DrawBrush = new SolidBrush(FillColor);
            return fig;
        }
        public override Cursor GetCursor()
        {
            return new Square("", Side).HoverCursor;
        }

        public override void UpdateSize(double side, double dummy = 0)
        {
            Side = side;
        }
    }

    public interface IDrawable
    {
        Pen DrawPen { get; set; }
        Brush DrawBrush { get; set; }
        void Draw(Graphics g);
    }

    public interface ISelectable
    {
        bool IsSelected { get; set; }
        Pen SelectionPen { get; set; }
        SelectionBubbles SelectionBubble { get; set; }
        void Select();
        void Deselect();
    }

    public struct Index2
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Index2(int x, int y)                                                                                                                                                                                                                                              
        {
            X = x; Y = y;
        }
    }

    public enum BubblePosition
    {
        NW = 0, NE, SE, SW
    }

    public class SelectionBubbles : IDrawable
    {
        public Pen DrawPen { get; set; } = Pens.Black;
        public Brush DrawBrush { get; set; } = new SolidBrush(Color.LightYellow);
        public List<RectangleSys> BubblesBoundRects { get; private set; }
        public Figure RelatedFigure { get; private set; }
        public RectangleSys TopLeftBubbleBoundRect { get; private set; }
        public RectangleSys TopRightBubbleBoundRect { get; private set; }
        public RectangleSys BotRightBubbleBoundRect { get; private set; }
        public RectangleSys BotLeftBubbleBoundRect { get; private set; }
        private const int _bubbleRadius = 6;
        private bool _mouseHovered = false;
        private bool _draggingActive = false;
        private BubblePosition _hoveredBubble;
        private Point _initialDraggingPoint;
        private Cursor _currentResizingCursor;
        public SelectionBubbles(Figure figure)
        {
            RectangleSys figureRect = figure.BoundRect;
            TopLeftBubbleBoundRect = new RectangleSys(figureRect.X - _bubbleRadius / 2, figureRect.Y - _bubbleRadius / 2,
                                    _bubbleRadius, _bubbleRadius);
            TopRightBubbleBoundRect = new RectangleSys(figureRect.Right - _bubbleRadius / 2, figureRect.Y - _bubbleRadius / 2,
                                    _bubbleRadius, _bubbleRadius);
            BotRightBubbleBoundRect = new RectangleSys(figureRect.Right - _bubbleRadius / 2, figureRect.Bottom - _bubbleRadius / 2,
                                    _bubbleRadius, _bubbleRadius);
            BotLeftBubbleBoundRect = new RectangleSys(figureRect.X - _bubbleRadius / 2, figureRect.Bottom - _bubbleRadius / 2,
                                    _bubbleRadius, _bubbleRadius);
            BubblesBoundRects = new List<RectangleSys>()
            {   TopLeftBubbleBoundRect,
                TopRightBubbleBoundRect,
                BotRightBubbleBoundRect,
                BotLeftBubbleBoundRect
            };
            RelatedFigure = figure;
        }

        /// <summary>
        /// Implies Plot "OnMouseMove" event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMouseOver(object sender, MouseEventArgs e)
        {
            if (!_draggingActive)
            {
                if (BubblesBoundRects.Any(x => x.Contains(e.Location)))
                {
                    _mouseHovered = true;
                    if (TopLeftBubbleBoundRect.Contains(e.Location))
                    {
                        Cursor.Current = Cursors.SizeNWSE;
                        _hoveredBubble = BubblePosition.NW;
                    }
                    else if (BotRightBubbleBoundRect.Contains(e.Location))
                    {
                        Cursor.Current = Cursors.SizeNWSE;
                        _hoveredBubble = BubblePosition.SE;
                    }
                    else if (TopRightBubbleBoundRect.Contains(e.Location))
                    {
                        Cursor.Current = Cursors.SizeNESW;
                        _hoveredBubble = BubblePosition.NE;
                    }
                    else if (BotLeftBubbleBoundRect.Contains(e.Location))
                    {
                        Cursor.Current = Cursors.SizeNESW;
                        _hoveredBubble = BubblePosition.SW;
                    }
                    _currentResizingCursor = Cursor.Current;
                }
                else
                    _mouseHovered = false;
            }
            else
            {
                Plot plt = (Plot)sender;

                Point finalDraggingPoint = e.Location;
                RectangleSys newBubbleRect = new RectangleSys(finalDraggingPoint.X - _bubbleRadius / 2, finalDraggingPoint.Y - _bubbleRadius / 2,
                                                                _bubbleRadius, _bubbleRadius);
                switch (_hoveredBubble)
                {
                    case BubblePosition.NW:
                        TopRightBubbleBoundRect = new RectangleSys(TopRightBubbleBoundRect.X, newBubbleRect.Y, _bubbleRadius, _bubbleRadius);
                        BotLeftBubbleBoundRect = new RectangleSys(newBubbleRect.X, BotLeftBubbleBoundRect.Y, _bubbleRadius, _bubbleRadius);
                        TopLeftBubbleBoundRect = newBubbleRect;
                        break;
                    case BubblePosition.NE:
                        TopLeftBubbleBoundRect = new RectangleSys(TopLeftBubbleBoundRect.X, newBubbleRect.Y, _bubbleRadius, _bubbleRadius);
                        BotRightBubbleBoundRect = new RectangleSys(newBubbleRect.X, BotRightBubbleBoundRect.Y, _bubbleRadius, _bubbleRadius);
                        TopRightBubbleBoundRect = newBubbleRect;
                        break;
                    case BubblePosition.SE:
                        BotLeftBubbleBoundRect = new RectangleSys(BotLeftBubbleBoundRect.X, newBubbleRect.Y, _bubbleRadius, _bubbleRadius);
                        TopRightBubbleBoundRect = new RectangleSys(newBubbleRect.X, TopRightBubbleBoundRect.Y, _bubbleRadius, _bubbleRadius);
                        BotRightBubbleBoundRect = newBubbleRect;
                        break;
                    case BubblePosition.SW:
                        TopLeftBubbleBoundRect = new RectangleSys(newBubbleRect.X, TopLeftBubbleBoundRect.Y, _bubbleRadius, _bubbleRadius);
                        BotRightBubbleBoundRect = new RectangleSys(BotRightBubbleBoundRect.X, newBubbleRect.Y, _bubbleRadius, _bubbleRadius);
                        BotLeftBubbleBoundRect = newBubbleRect;
                        break;
                }
                BubblesBoundRects.Clear();
                BubblesBoundRects.AddRange(new RectangleSys[] {
                    TopLeftBubbleBoundRect, TopRightBubbleBoundRect, BotRightBubbleBoundRect, BotLeftBubbleBoundRect
                });
                RectangleSys newBoundRectForFigure = RecalcNewFigureBoundRect();
                RelatedFigure.Resize(newBoundRectForFigure);

                plt.Invalidate();
            }
        }

        public void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (_mouseHovered && e.Button == MouseButtons.Left)
            {
                _draggingActive = true;
                Cursor.Current = _currentResizingCursor;
                _initialDraggingPoint = e.Location;
            }
        }
        
        public void OnMouseUp(object sender, MouseEventArgs e)
        {
            if (_draggingActive)
                _draggingActive = false;
        }

        private RectangleSys RecalcNewFigureBoundRect()
        {
            return new RectangleSys(TopLeftBubbleBoundRect.X + _bubbleRadius / 2,
                                    TopLeftBubbleBoundRect.Y + _bubbleRadius / 2,
                                    TopRightBubbleBoundRect.X - TopLeftBubbleBoundRect.X,
                                    BotLeftBubbleBoundRect.Y - TopLeftBubbleBoundRect.Y);
        }

        public void Draw(Graphics g)
        {
            foreach (RectangleSys bubbleRect in BubblesBoundRects)
            {
                g.FillEllipse(DrawBrush, bubbleRect);
                g.DrawEllipse(DrawPen, bubbleRect);
            }
        }
    }

    public abstract class Figure : IDisposable, IDrawable, ISelectable
    {
        public string Name { get; private set; } = "fig";
        public abstract string DefaultName { get; protected set; }
        public Index2 Center { get; private set; } = new Index2(0, 0);
        public RectangleSys BoundRect { get; set; }
        public bool IsFilled { get; set; } = false;
        public Cursor HoverCursor { get; protected set; }
        public Color FillColor { get; set; } = Color.Black;
        public Color ContourColor { get; set; } = Color.Black;
        public SelectionBubbles SelectionBubble { get; set; }
        protected bool _resized = false;
        public bool IsSelected { get; set; } = false;
        public event FigureStateChange FigureStateChanged;
        public Pen SelectionPen { get; set; } = new Pen(Color.LightGray)
        {
            DashStyle = System.Drawing.Drawing2D.DashStyle.Dash
        };
        public Pen DrawPen { get; set; } = new Pen(Color.Black)
        {
            Width = 2
        };
        public Brush DrawBrush { get; set; } = Brushes.Black;
        public Figure(string name)
        {
            Name = name;
        }
        public abstract void Draw(Graphics g);
        protected virtual void DrawSelectionDesign(Graphics g)
        {
            g.DrawRectangle(SelectionPen, BoundRect);
            SelectionBubble.Draw(g);
        }
        public void Resize(RectangleSys boundRect)
        {
            BoundRect = boundRect;
            Center = new Index2(boundRect.X + boundRect.Width / 2,
                                    boundRect.Y + boundRect.Height / 2);
            UpdateGeometricalParams(boundRect);
            _resized = true;
            FigureStateChanged?.Invoke(this, new FigureStateChangedEventArgs() { IsSelected = null,
                                                                                IsResized = true });
        }
        protected abstract void UpdateGeometricalParams(RectangleSys newBoundRect);
        protected abstract Cursor CreateCursor();
        public void SetCenterCoords(int x, int y)
        {
            Center = new Index2(x, y);
        }
        public void Select()
        {
            IsSelected = true;
            SelectionBubble = new SelectionBubbles(this);
            FigureStateChanged?.Invoke(this, new FigureStateChangedEventArgs() {
                IsSelected = IsSelected
            });
        }
        public void Deselect()
        {
            IsSelected = false;
            FigureStateChanged?.Invoke(this, new FigureStateChangedEventArgs() {
                IsSelected = IsSelected
            });
        }
        public abstract Bitmap CreateCurrentIcon(int width = 32, int height = 32);

        public void Dispose()
        {
            HoverCursor = null;
            SelectionBubble = null;
            FigureStateChanged = null;
            SelectionPen = null;
            DrawPen = null;
            DrawBrush = null;
        }
    }

    public class Circle: Figure
    {
        public double Radius { get; set; } = 10;
        public static string Tag { get; private set; } = "Circle";
        public override string DefaultName { get; protected set; } = "Circle";
        public Circle(string name): base(name)
        {

        }
        public Circle(string name, double r): base(name)
        {
            Radius = r; 
            HoverCursor = CreateCursor();
        }

        protected override Cursor CreateCursor()
        {
            Bitmap bmp = new Bitmap((int)Radius, (int)Radius);
            using (Graphics g = Graphics.FromImage(bmp))
                g.DrawEllipse(Pens.Black, 0, 0, bmp.Width - 1, bmp.Height - 1);
            return new Cursor(bmp.GetHicon());
        }

        public override void Draw(Graphics g)
        {
            RectangleSys rect;
            if (!_resized)
            {
                rect = new RectangleSys(Center.X - (int)(Radius / 2), Center.Y - (int)(Radius / 2),
                                            (int)((float)Radius - 1), (int)((float)Radius - 1));
                BoundRect = rect;
            }
            else
            {
                rect = BoundRect;
                _resized = false;
            }


            if (IsFilled)
                g.FillEllipse(DrawBrush, rect);

            g.DrawEllipse(DrawPen, rect);

            if (IsSelected)
                DrawSelectionDesign(g);
        }

        public static Bitmap CreateDefaultIcon(int width = 32, int height = 32)
        {
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(bmp))
                g.DrawEllipse(Pens.Black, 0, 0, width - 1, height - 1);
            return bmp;
        }

        public override Bitmap CreateCurrentIcon(int width = 32, int height = 32)
        {
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                if (IsFilled)
                    g.FillEllipse(DrawBrush, 0, 0, width - 1, height - 1);

                g.DrawEllipse(DrawPen, 0, 0, width - 1, height - 1);
            }
            return bmp;
        }

        protected override void UpdateGeometricalParams(RectangleSys newBoundRect)
        {
            Radius = newBoundRect.Height;
        }
    }

    public class Rectangle: Figure
    {
        public double VerSide { get; set; } = 5;
        public double HorSide { get; set; } = 10;
        public static string Tag { get; private set; } = "Rectangle";
        public override string DefaultName { get; protected set; } = "Rectangle";

        public Rectangle(string name): base(name)
        {

        }
        public Rectangle(string name, double verSide, double horSide): base(name)
        {
            VerSide = verSide;
            HorSide = horSide;
            HoverCursor = CreateCursor();
        }

        protected override Cursor CreateCursor()
        {
            Bitmap bmp = new Bitmap((int)HorSide, (int)VerSide);
            using (Graphics g = Graphics.FromImage(bmp))
                g.DrawRectangle(Pens.Black, 0, 0, bmp.Width - 1, bmp.Height - 1);
            return new Cursor(bmp.GetHicon());
        }

        public override void Draw(Graphics g)
        {
            RectangleSys rect;
            if (!_resized) {
                rect = new RectangleSys(Center.X - (int)(HorSide / 2),
                                        Center.Y - (int)(VerSide / 2), (int)((float)HorSide - 1), (int)((float)VerSide - 1));
                BoundRect = rect;
            }
            else
            {
                rect = BoundRect;
                _resized = false;
            }


            if (IsFilled)
                g.FillRectangle(DrawBrush, rect);
            
            g.DrawRectangle(DrawPen, rect);

            if (IsSelected)
                DrawSelectionDesign(g);
        }

        public static Bitmap CreateDefaultIcon(int width = 32, int height = 32)
        {
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(bmp))
                g.DrawRectangle(Pens.Black, 0, (int)((float)height / 4), width - 1, (int)((float)height / 2 - 1));
            return bmp;
        }

        public override Bitmap CreateCurrentIcon(int width = 32, int height = 32)
        {
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                if (IsFilled)
                    g.FillRectangle(DrawBrush, 0, (int)((float)height / 4), width - 1, (int)((float)height / 2 - 1));

                g.DrawRectangle(DrawPen, 0, (int)((float)height / 4), width - 1, (int)((float)height / 2 - 1));
            }
            return bmp;
        }

        protected override void UpdateGeometricalParams(RectangleSys newBoundRect)
        {
            HorSide = newBoundRect.Width;
            VerSide = newBoundRect.Height;
        }
    }

    public class Square: Figure
    {
        public double Side { get; set; } = 12;
        public static string Tag { get; private set; } = "Square";
        public override string DefaultName { get; protected set; } = "Square";

        public Square(string name): base(name)
        {

        }
        public Square(string name, double side): base(name)
        {
            Side = side;
            HoverCursor = CreateCursor();
        }

        protected override Cursor CreateCursor()
        {
            Bitmap bmp = new Bitmap((int)Side, (int)Side);
            using (Graphics g = Graphics.FromImage(bmp))
                g.DrawRectangle(Pens.Black, 0, 0, bmp.Width - 1, bmp.Height - 1);
            return new Cursor(bmp.GetHicon());
        }

        public override void Draw(Graphics g)
        {
            RectangleSys rect;
            if (!_resized)
            {
                rect = new RectangleSys(Center.X - (int)(Side / 2),
                                        Center.Y - (int)(Side / 2), (int)((float)Side - 1), (int)((float)Side - 1));
                BoundRect = rect;
            }
            else
            {
                rect = BoundRect;
                _resized = false;
            }


            if (IsFilled)
                g.FillRectangle(DrawBrush, rect);
            
            g.DrawRectangle(DrawPen, rect);

            if (IsSelected)
                DrawSelectionDesign(g);
        }

        public static Bitmap CreateDefaultIcon(int width = 32, int height = 32)
        {
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(bmp))
                g.DrawRectangle(Pens.Black, 0, 0, width - 1, height - 1);
            return bmp;
        }

        public override Bitmap CreateCurrentIcon(int width = 32, int height = 32)
        {
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                if (IsFilled)
                    g.FillRectangle(DrawBrush, 0, 0, width - 1, height - 1);
                g.DrawRectangle(DrawPen, 0, 0, width - 1, height - 1);
            }
            return bmp;
        }

        protected override void UpdateGeometricalParams(RectangleSys newBoundRect)
        {
            Side = newBoundRect.Height;
        }
    }
}
