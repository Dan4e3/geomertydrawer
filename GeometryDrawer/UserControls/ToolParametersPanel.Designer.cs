﻿namespace GeometryDrawer.UserControls
{
    partial class ToolParametersPanel
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.SizeTabPage = new System.Windows.Forms.TabPage();
            this.VerSizeLabel = new System.Windows.Forms.Label();
            this.VerticalSizeTrackBar = new System.Windows.Forms.TrackBar();
            this.HorSizeLabel = new System.Windows.Forms.Label();
            this.HorizontalSizeTrackBar = new System.Windows.Forms.TrackBar();
            this.ColorTabPage = new System.Windows.Forms.TabPage();
            this.FillColorLabelInfo = new System.Windows.Forms.Label();
            this.FillColorLabel = new System.Windows.Forms.Label();
            this.AdditionalParamsTabPage = new System.Windows.Forms.TabPage();
            this.IsFilledCheckBox = new System.Windows.Forms.CheckBox();
            this.ColorDialog = new System.Windows.Forms.ColorDialog();
            this.ContourColorLabelInfo = new System.Windows.Forms.Label();
            this.ContourColorLabel = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.SizeTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VerticalSizeTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HorizontalSizeTrackBar)).BeginInit();
            this.ColorTabPage.SuspendLayout();
            this.AdditionalParamsTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.SizeTabPage);
            this.tabControl1.Controls.Add(this.ColorTabPage);
            this.tabControl1.Controls.Add(this.AdditionalParamsTabPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(379, 105);
            this.tabControl1.TabIndex = 0;
            // 
            // SizeTabPage
            // 
            this.SizeTabPage.Controls.Add(this.VerSizeLabel);
            this.SizeTabPage.Controls.Add(this.VerticalSizeTrackBar);
            this.SizeTabPage.Controls.Add(this.HorSizeLabel);
            this.SizeTabPage.Controls.Add(this.HorizontalSizeTrackBar);
            this.SizeTabPage.Location = new System.Drawing.Point(4, 22);
            this.SizeTabPage.Name = "SizeTabPage";
            this.SizeTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.SizeTabPage.Size = new System.Drawing.Size(371, 79);
            this.SizeTabPage.TabIndex = 0;
            this.SizeTabPage.Text = "Size";
            this.SizeTabPage.UseVisualStyleBackColor = true;
            // 
            // VerSizeLabel
            // 
            this.VerSizeLabel.AutoSize = true;
            this.VerSizeLabel.Location = new System.Drawing.Point(276, 30);
            this.VerSizeLabel.Name = "VerSizeLabel";
            this.VerSizeLabel.Size = new System.Drawing.Size(35, 13);
            this.VerSizeLabel.TabIndex = 3;
            this.VerSizeLabel.Text = "label1";
            // 
            // VerticalSizeTrackBar
            // 
            this.VerticalSizeTrackBar.Location = new System.Drawing.Point(191, 8);
            this.VerticalSizeTrackBar.Maximum = 96;
            this.VerticalSizeTrackBar.Minimum = 6;
            this.VerticalSizeTrackBar.Name = "VerticalSizeTrackBar";
            this.VerticalSizeTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.VerticalSizeTrackBar.Size = new System.Drawing.Size(45, 60);
            this.VerticalSizeTrackBar.TabIndex = 2;
            this.VerticalSizeTrackBar.TickFrequency = 6;
            this.VerticalSizeTrackBar.Value = 6;
            this.VerticalSizeTrackBar.Scroll += new System.EventHandler(this.VerticalSizeTrackBar_Scroll);
            // 
            // HorSizeLabel
            // 
            this.HorSizeLabel.AutoSize = true;
            this.HorSizeLabel.Location = new System.Drawing.Point(83, 30);
            this.HorSizeLabel.Name = "HorSizeLabel";
            this.HorSizeLabel.Size = new System.Drawing.Size(35, 13);
            this.HorSizeLabel.TabIndex = 1;
            this.HorSizeLabel.Text = "label1";
            // 
            // HorizontalSizeTrackBar
            // 
            this.HorizontalSizeTrackBar.Location = new System.Drawing.Point(6, 16);
            this.HorizontalSizeTrackBar.Maximum = 96;
            this.HorizontalSizeTrackBar.Minimum = 6;
            this.HorizontalSizeTrackBar.Name = "HorizontalSizeTrackBar";
            this.HorizontalSizeTrackBar.Size = new System.Drawing.Size(71, 45);
            this.HorizontalSizeTrackBar.TabIndex = 0;
            this.HorizontalSizeTrackBar.TickFrequency = 6;
            this.HorizontalSizeTrackBar.Value = 6;
            this.HorizontalSizeTrackBar.Scroll += new System.EventHandler(this.HorizontalSizeTrackBar_Scroll);
            // 
            // ColorTabPage
            // 
            this.ColorTabPage.Controls.Add(this.ContourColorLabelInfo);
            this.ColorTabPage.Controls.Add(this.ContourColorLabel);
            this.ColorTabPage.Controls.Add(this.FillColorLabelInfo);
            this.ColorTabPage.Controls.Add(this.FillColorLabel);
            this.ColorTabPage.Location = new System.Drawing.Point(4, 22);
            this.ColorTabPage.Name = "ColorTabPage";
            this.ColorTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.ColorTabPage.Size = new System.Drawing.Size(371, 79);
            this.ColorTabPage.TabIndex = 1;
            this.ColorTabPage.Text = "Color";
            this.ColorTabPage.UseVisualStyleBackColor = true;
            // 
            // FillColorLabelInfo
            // 
            this.FillColorLabelInfo.AutoSize = true;
            this.FillColorLabelInfo.Location = new System.Drawing.Point(6, 10);
            this.FillColorLabelInfo.Name = "FillColorLabelInfo";
            this.FillColorLabelInfo.Size = new System.Drawing.Size(45, 13);
            this.FillColorLabelInfo.TabIndex = 2;
            this.FillColorLabelInfo.Text = "Fill color";
            // 
            // FillColorLabel
            // 
            this.FillColorLabel.BackColor = System.Drawing.Color.Black;
            this.FillColorLabel.Location = new System.Drawing.Point(6, 23);
            this.FillColorLabel.Name = "FillColorLabel";
            this.FillColorLabel.Size = new System.Drawing.Size(88, 41);
            this.FillColorLabel.TabIndex = 0;
            this.FillColorLabel.Click += new System.EventHandler(this.FillColorLabel_Click);
            // 
            // AdditionalParamsTabPage
            // 
            this.AdditionalParamsTabPage.Controls.Add(this.IsFilledCheckBox);
            this.AdditionalParamsTabPage.Location = new System.Drawing.Point(4, 22);
            this.AdditionalParamsTabPage.Name = "AdditionalParamsTabPage";
            this.AdditionalParamsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.AdditionalParamsTabPage.Size = new System.Drawing.Size(371, 79);
            this.AdditionalParamsTabPage.TabIndex = 2;
            this.AdditionalParamsTabPage.Text = "Others";
            this.AdditionalParamsTabPage.UseVisualStyleBackColor = true;
            // 
            // IsFilledCheckBox
            // 
            this.IsFilledCheckBox.AutoSize = true;
            this.IsFilledCheckBox.Location = new System.Drawing.Point(3, 6);
            this.IsFilledCheckBox.Name = "IsFilledCheckBox";
            this.IsFilledCheckBox.Size = new System.Drawing.Size(89, 17);
            this.IsFilledCheckBox.TabIndex = 0;
            this.IsFilledCheckBox.Text = "Figure is filled";
            this.IsFilledCheckBox.UseVisualStyleBackColor = true;
            this.IsFilledCheckBox.CheckedChanged += new System.EventHandler(this.IsFilledCheckBox_CheckedChanged);
            // 
            // ContourColorLabelInfo
            // 
            this.ContourColorLabelInfo.AutoSize = true;
            this.ContourColorLabelInfo.Location = new System.Drawing.Point(222, 10);
            this.ContourColorLabelInfo.Name = "ContourColorLabelInfo";
            this.ContourColorLabelInfo.Size = new System.Drawing.Size(70, 13);
            this.ContourColorLabelInfo.TabIndex = 4;
            this.ContourColorLabelInfo.Text = "Contour color";
            // 
            // ContourColorLabel
            // 
            this.ContourColorLabel.BackColor = System.Drawing.Color.Black;
            this.ContourColorLabel.Location = new System.Drawing.Point(222, 23);
            this.ContourColorLabel.Name = "ContourColorLabel";
            this.ContourColorLabel.Size = new System.Drawing.Size(88, 41);
            this.ContourColorLabel.TabIndex = 3;
            this.ContourColorLabel.Click += new System.EventHandler(this.ContourColorLabel_Click);
            // 
            // ToolParametersPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Name = "ToolParametersPanel";
            this.Size = new System.Drawing.Size(379, 105);
            this.tabControl1.ResumeLayout(false);
            this.SizeTabPage.ResumeLayout(false);
            this.SizeTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VerticalSizeTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HorizontalSizeTrackBar)).EndInit();
            this.ColorTabPage.ResumeLayout(false);
            this.ColorTabPage.PerformLayout();
            this.AdditionalParamsTabPage.ResumeLayout(false);
            this.AdditionalParamsTabPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage SizeTabPage;
        private System.Windows.Forms.TabPage ColorTabPage;
        private System.Windows.Forms.Label HorSizeLabel;
        private System.Windows.Forms.TrackBar HorizontalSizeTrackBar;
        private System.Windows.Forms.Label VerSizeLabel;
        private System.Windows.Forms.TrackBar VerticalSizeTrackBar;
        private System.Windows.Forms.TabPage AdditionalParamsTabPage;
        private System.Windows.Forms.CheckBox IsFilledCheckBox;
        private System.Windows.Forms.Label FillColorLabel;
        private System.Windows.Forms.ColorDialog ColorDialog;
        private System.Windows.Forms.Label FillColorLabelInfo;
        private System.Windows.Forms.Label ContourColorLabelInfo;
        private System.Windows.Forms.Label ContourColorLabel;
    }
}
