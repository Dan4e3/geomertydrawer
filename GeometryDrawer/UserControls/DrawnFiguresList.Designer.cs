﻿namespace GeometryDrawer.UserControls
{
    partial class DrawnFiguresList
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.DrawnFiguresListView = new System.Windows.Forms.ListView();
            this.FigureColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // DrawnFiguresListView
            // 
            this.DrawnFiguresListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.FigureColumn});
            this.DrawnFiguresListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DrawnFiguresListView.FullRowSelect = true;
            this.DrawnFiguresListView.HideSelection = false;
            this.DrawnFiguresListView.Location = new System.Drawing.Point(0, 0);
            this.DrawnFiguresListView.Name = "DrawnFiguresListView";
            this.DrawnFiguresListView.Size = new System.Drawing.Size(190, 383);
            this.DrawnFiguresListView.TabIndex = 0;
            this.DrawnFiguresListView.UseCompatibleStateImageBehavior = false;
            this.DrawnFiguresListView.View = System.Windows.Forms.View.Details;
            this.DrawnFiguresListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.DrawnFiguresListView_ItemSelectionChanged);
            this.DrawnFiguresListView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DrawnFiguresListView_KeyDown);
            // 
            // FigureColumn
            // 
            this.FigureColumn.Text = "Figure";
            this.FigureColumn.Width = 180;
            // 
            // DrawnFiguresList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DrawnFiguresListView);
            this.Name = "DrawnFiguresList";
            this.Size = new System.Drawing.Size(190, 383);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView DrawnFiguresListView;
        private System.Windows.Forms.ColumnHeader FigureColumn;
    }
}
