﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeometryDrawer.UserControls
{
    public partial class ActionToolPanel : UserControl
    {
        public Plot RelatedPlot { get; private set; }
        public IActionTool ActiveTool { get; private set; } = null;
        public event EventHandler ActionToolSelectionChanged;

        public ActionToolPanel()
        {
            InitializeComponent();
        }

        public void SetRelatedPlot(Plot plt)
        {
            RelatedPlot = plt;
        }

        private void SelectToolButton_Click(object sender, EventArgs e)
        {
            ActiveTool = SelectTool.GetToolInstance();
            ActionToolSelectionChanged?.Invoke(this, new EventArgs());
        }

        public void DeactivateTool()
        {
            ActiveTool = null;
        }
    }

    public class ActionToolArgsPack
    {
        public Point PlotClickedPoint { get; set; }
        public List<Figure> FiguresList { get; set; }
    }

    public interface IActionTool
    {
        void ApplyAction(ActionToolArgsPack argsPack);
    }

    public class SelectTool : IActionTool
    {
        private static SelectTool _selectToolInstance = null;

        public void ApplyAction(ActionToolArgsPack argsPack)
        {
            Figure fig = GetFigureByPointClick(argsPack.PlotClickedPoint, argsPack.FiguresList);
            // if clicked selection bubble - do nothing
            if (fig?.SelectionBubble?.BubblesBoundRects?.Any(rect => rect.Contains(argsPack.PlotClickedPoint)) == true)
                return;
            if (fig != null)
            {
                if (fig.IsSelected)
                    fig.Deselect();
                else
                    fig.Select();
            }
        }

        private Figure GetClosestFigure(Point clickCoords, List<Figure> figuresList)
        {
            Figure closestFigure = null;
            double? minDistance = null;
            foreach (Figure fig in figuresList)
            {
                double distance = Math.Sqrt(Math.Pow(fig.Center.X - clickCoords.X, 2) +
                           Math.Pow(fig.Center.Y - clickCoords.Y, 2));
                if (closestFigure == null)
                    closestFigure = fig;

                if (minDistance == null)
                    minDistance = distance;
                else if (distance < minDistance)
                {
                    minDistance = distance;
                    closestFigure = fig;
                }
            }
            return closestFigure;
        }

        private Figure GetFigureByPointClick(Point clickCoords, List<Figure> figuresList)
        {
            foreach (Figure fig in figuresList)
            {
                int leftTopX = fig.BoundRect.X;
                int leftTopY = fig.BoundRect.Y;
                int rightBotX = fig.BoundRect.Right;
                int rightBotY = fig.BoundRect.Bottom;

                if (clickCoords.X >= leftTopX && clickCoords.X <= rightBotX &&
                    clickCoords.Y >= leftTopY && clickCoords.Y <= rightBotY)
                    return fig;
            }

            return null;
        }

        public static SelectTool GetToolInstance()
        {
            if (_selectToolInstance == null)
                _selectToolInstance = new SelectTool();
            return _selectToolInstance;
        }
    }
}
