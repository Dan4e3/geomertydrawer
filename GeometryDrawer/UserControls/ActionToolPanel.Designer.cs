﻿namespace GeometryDrawer.UserControls
{
    partial class ActionToolPanel
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectToolButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SelectToolButton
            // 
            this.SelectToolButton.Location = new System.Drawing.Point(0, 0);
            this.SelectToolButton.Name = "SelectToolButton";
            this.SelectToolButton.Size = new System.Drawing.Size(55, 41);
            this.SelectToolButton.TabIndex = 0;
            this.SelectToolButton.Text = "Select";
            this.SelectToolButton.UseVisualStyleBackColor = true;
            this.SelectToolButton.Click += new System.EventHandler(this.SelectToolButton_Click);
            // 
            // ActionToolPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SelectToolButton);
            this.Name = "ActionToolPanel";
            this.Size = new System.Drawing.Size(227, 41);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SelectToolButton;
    }
}
