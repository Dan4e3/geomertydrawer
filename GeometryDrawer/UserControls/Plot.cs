﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GeometryDrawer.UserControls;
using RectangleSys = System.Drawing.Rectangle;

namespace GeometryDrawer.UserControls
{
    public partial class Plot : UserControl
    {
        public GeometryToolPanel GeometryToolPan { get; private set; }
        public ToolParametersPanel ParametersPanel { get; private set; }
        public DrawnFiguresList DrawnFiguresControl { get; private set; }
        public ActionToolPanel ActionToolPan { get; private set; }
        private List<Figure> DrawnFigures { get; set; } = new List<Figure>();
        public delegate void FigureListChangedEventHandler(object sender, FigureEventArgs e);
        public event FigureListChangedEventHandler FigureAddedToPlot;

        public class FigureEventArgs: EventArgs
        {
            public Figure SubjectFigure { get; private set; }
            public FigureEventArgs(Figure fig)
            {
                SubjectFigure = fig;
            }
        }

        public Plot()
        {
            InitializeComponent();
        }

        public void ClearPlot()
        {
            DrawnFigures.Clear();
            DrawnFiguresControl.ClearList();
            this.Invalidate();
        }

        public void SetRelatedPanel(GeometryToolPanel geometryPanel, ToolParametersPanel paramsPanel,
            DrawnFiguresList drawnFigsPanel, ActionToolPanel actionToolPanel)
        {
            GeometryToolPan = geometryPanel;
            GeometryToolPan.GeometryToolSelectionChanged += GeometryToolPan_GeometryToolSelectionChanged;
            ParametersPanel = paramsPanel;
            DrawnFiguresControl = drawnFigsPanel;
            DrawnFiguresControl.FigureRemoved += DrawnFiguresControl_FigureRemoved;
            ActionToolPan = actionToolPanel;
            ActionToolPan.ActionToolSelectionChanged += ActionToolPan_ActionToolSelectionChanged;
        }

        private void ActionToolPan_ActionToolSelectionChanged(object sender, EventArgs e)
        {
            GeometryToolPan.DeactivateFactory();
        }

        private void GeometryToolPan_GeometryToolSelectionChanged(object sender, EventArgs e)
        {
            ActionToolPan.DeactivateTool();
        }

        private void DrawnFiguresControl_FigureRemoved(object sender, FigureEventArgs e)
        {
            using (Figure fig = e.SubjectFigure)
                DrawnFigures.Remove(fig);
            this.Invalidate();
        }

        private void AddFigureToPlot(Figure fig)
        {
            DrawnFigures.Add(fig);
            fig.FigureStateChanged += Fig_FigureStateChanged;
            FigureAddedToPlot?.Invoke(this, new FigureEventArgs(fig));
        }

        private void Plot_MouseEnter(object sender, EventArgs e)
        {
            if (GeometryToolPan.ActiveFactory != null)
                Cursor = GeometryToolPan.ActiveFactory.GetCursor();
        }

        private void Plot_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void Plot_MouseDown(object sender, MouseEventArgs e)
        {
            if (GeometryToolPan.ActiveFactory != null)
            {
                Figure fig = GeometryToolPan.ActiveFactory.CreateFigure();
                fig.SetCenterCoords(e.X, e.Y);
                AddFigureToPlot(fig);
                this.Invalidate();
            }

            ActionToolPan?.ActiveTool?.ApplyAction(new ActionToolArgsPack()
            {
                PlotClickedPoint = e.Location,
                FiguresList = DrawnFigures
            });
        }

        private void Fig_FigureStateChanged(object sender, FigureStateChangedEventArgs e)
        {
            Figure invokingFigure = (Figure)sender;
            if (e.IsSelected != null)
                DrawnFiguresControl.ChangeFigureItemState(invokingFigure, (bool)e.IsSelected);
            if (e.IsSelected == true)
            {
                MouseMove += invokingFigure.SelectionBubble.OnMouseOver;
                MouseDown += invokingFigure.SelectionBubble.OnMouseDown;
                MouseUp += invokingFigure.SelectionBubble.OnMouseUp;
            }
            else if (e.IsSelected == false)
            {
                MouseMove -= invokingFigure.SelectionBubble.OnMouseOver;
                MouseDown -= invokingFigure.SelectionBubble.OnMouseDown;
                MouseUp -= invokingFigure.SelectionBubble.OnMouseUp;
            }
                
            this.Invalidate();
        }

        private void Plot_Paint(object sender, PaintEventArgs e)
        {
            foreach (Figure fig in DrawnFigures)
            {
                    fig.Draw(e.Graphics);
            }
        }

        private void Plot_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                List<Figure> removalList = new List<Figure>();
                removalList.AddRange(
                    DrawnFigures.Where(fig => fig.IsSelected == true)
                    );
                foreach (Figure fig in removalList)
                    DrawnFiguresControl.RemoveFigureFromList(fig);
            }
        }
    }
    
}
