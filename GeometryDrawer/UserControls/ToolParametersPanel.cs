﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeometryDrawer.UserControls
{
    public partial class ToolParametersPanel : UserControl
    {
        private GeometryToolPanel ToolPanel { get; set; }
        public double HorSize { get; private set; } = 10;
        public double VerSize { get; private set; } = 6;
        public bool IsFilled { get; private set; } = false;
        public Color SelectedFillColor { get; private set; } = Color.Black;
        public Color SelectedContourColor { get; private set; } = Color.Black;

        public ToolParametersPanel()
        {
            InitializeComponent();
            HorSizeLabel.Text = HorSize.ToString();
            VerSizeLabel.Text = VerSize.ToString();
            HorizontalSizeTrackBar.Minimum = 6;
            VerticalSizeTrackBar.Minimum = 6;
            IsFilledCheckBox.Checked = IsFilled;
            FillColorLabel.BackColor = SelectedFillColor;
            ContourColorLabel.BackColor = SelectedContourColor;
        }

        public void SetRelatedGeometryToolPanel(GeometryToolPanel toolPan)
        {
            ToolPanel = toolPan;
        }

        private void HorizontalSizeTrackBar_Scroll(object sender, EventArgs e)
        {
            HorSizeLabel.Text = HorizontalSizeTrackBar.Value.ToString();
            HorSize = HorizontalSizeTrackBar.Value;
            ToolPanel?.ActiveFactory?.UpdateSize(HorizontalSizeTrackBar.Value, VerticalSizeTrackBar.Value);
        }

        private void VerticalSizeTrackBar_Scroll(object sender, EventArgs e)
        {
            VerSizeLabel.Text = VerticalSizeTrackBar.Value.ToString();
            VerSize = VerticalSizeTrackBar.Value;
            ToolPanel?.ActiveFactory?.UpdateSize(HorizontalSizeTrackBar.Value, VerticalSizeTrackBar.Value);
        }

        private void IsFilledCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            IsFilled = IsFilledCheckBox.Checked;
            ToolPanel?.ActiveFactory?.UpdateFill(IsFilled);
        }

        private void FillColorLabel_Click(object sender, EventArgs e)
        {
            if (ColorDialog.ShowDialog() == DialogResult.OK)
            {
                FillColorLabel.BackColor = ColorDialog.Color;
                SelectedFillColor = ColorDialog.Color;
                ToolPanel?.ActiveFactory?.UpdateFillColor(ColorDialog.Color);
            }
        }

        private void ContourColorLabel_Click(object sender, EventArgs e)
        {
            if (ColorDialog.ShowDialog() == DialogResult.OK)
            {
                ContourColorLabel.BackColor = ColorDialog.Color;
                SelectedContourColor = ColorDialog.Color;
                ToolPanel?.ActiveFactory?.UpdateContourColor(ColorDialog.Color);
            }
        }
    }
}
