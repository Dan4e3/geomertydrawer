﻿namespace GeometryDrawer.UserControls
{
    partial class Plot
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Plot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.DoubleBuffered = true;
            this.Name = "Plot";
            this.Size = new System.Drawing.Size(312, 293);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Plot_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Plot_KeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Plot_MouseDown);
            this.MouseEnter += new System.EventHandler(this.Plot_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.Plot_MouseLeave);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
